<title>CS202 Lecture Notes</title>
<h2>CS202 Lecture Notes - Unordered Sets and Maps</h2>
<UL>
<LI> James S. Plank
<LI> Original Notes: March 24, 2023
<LI> Last revision: <i>
Fri Mar 24 11:13:27 EDT 2023
</i>
<LI> Directory: <b>/home/jplank/cs202/Notes/Unordered</b>
</UL>

<hr>
<h3> Introduction</h3>

C++11 introduced a very powerful set of data structures: <b>unordered_set</b> and
<b>unordered_map</b>.  These are the exact same as <b>set</b> and <b>map</b>, except for
one key difference -- they are not sorted.  
<p>
That means that if you iterate through them, they will be in any arbitrary order.
What you gain for that difference is speed:
<p>
<center><table border=3 cellpadding=5>
<tr>
<td align=center><b>Operation</b></td>
<td align=center><b>Set/Map</b></td>
<td align=center><b>Unordered Set/Map</b></td>
</tr>
<tr>
<td align=center><tt>insert()</tt></td>
<td align=center><i>O(log n)</i></td>
<td align=center><i>O(1)</i></td>
</tr>
<tr>
<td align=center><tt>find()</tt></td>
<td align=center><i>O(log n)</i></td>
<td align=center><i>O(1)</i></td>
</tr>
<tr>
<td align=center><tt>erase()</tt></td>
<td align=center><i>O(log n)</i></td>
<td align=center><i>O(1)</i></td>
</tr>
<tr>
<td align=center><tt>begin()</tt></td>
<td align=center><i>O(1)</i></td>
<td align=center><i>O(1)</i></td>
</tr>
<tr>
<td align=center><tt>end()</tt></td>
<td align=center><i>O(1)</i></td>
<td align=center><i>O(1)</i></td>
</tr>
<tr>
<td align=center>Traversal</td>
<td align=center><i>O(n)</i></td>
<td align=center><i>O(n)</i></td>
</tr>
</table><p></center>

They are implemented using a hash table that gets resized and rehashed when it becomes too full.
So, if you don't need the sorting feature of a set or map, you should use the unordered data
structures.

<hr>
<h3>An example</h3>

I have two very simple programs in 
<b><a href=src/store_find_set.cpp>src/store_find_set.cpp</a></b> and
<b><a href=src/store_find_unordered.cpp>src/store_find_unordered.cpp</a></b>.  They are
the exact same, except the first one uses a set and the second uses an unordered set.
<p>
The programs take two files as arguments and a "Y/N" for printing.  They read the words in 
the first file into a set or unordered set.  Then they read words in the second file and
try to find them.  If you said "Y" for printing, then it prints everything.  Otherwise, it
simply prints how many words it found.  
<p>
The code is straightforward, and you should have no trouble reading it:

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;set&gt;
#include &lt;iostream&gt;
#include &lt;fstream&gt;
using namespace std;

int main(int argc, char **argv)
{
  ifstream data_file, to_find_file;
  bool print;
  set &lt;string&gt; data;
  set &lt;string&gt;::const_iterator f;
  string s;
  int found;

  <font color=blue>/* Parse the command line. */</font>

  try {
    if (argc != 4) throw (string) "usage: store_find_set data_file to_find_file print(Y/N)\n";
    data_file.open(argv[1]);
    if (data_file.fail()) throw (string) "can't open " + argv[1];
    to_find_file.open(argv[2]);
    if (to_find_file.fail()) throw (string) "can't open " + argv[2];
    print = (argv[3][0] == 'Y');
  } catch (const string &s) {
    cerr &lt;&lt; s &lt;&lt; endl;
    return 1;
  }
 
  <font color=blue>/* Read the data file. */</font>

  while (data_file &gt;&gt; s) data.insert(s);

  if (print) {
    cout &lt;&lt; "Data:" &lt;&lt; endl;
    for (f = data.begin(); f != data.end(); f++) cout &lt;&lt; *f &lt;&lt; endl;
    cout &lt;&lt; endl;
  }
 
  data_file.close();

  <font color=blue>/* Read the to_find_file, and try to find each word in the data file */</font>

  found = 0;

  while (to_find_file &gt;&gt; s) {
    f = data.find(s);
    if (f != data.end()) found++;
    if (print) cout &lt;&lt; s &lt;&lt; ": " &lt;&lt; ((f == data.end()) ? "Not found" : "Found") &lt;&lt; endl;
  }

  if (print) cout &lt;&lt; endl;
  cout &lt;&lt; "Found " &lt;&lt; found &lt;&lt; endl;
  return 0;
}
</pre></td></table></center><p>

First, let's run it and look at output.  I have the following files:

<UL>
<LI> <b><a href=txt/phones-small.txt>txt/phones-small.txt</a></b> - Ten random phone numbers.
<LI> <b><a href=txt/phones-big.txt>txt/phones-big.txt</a></b> - 100,000 random phone numbers.
<LI> <b><a href=txt/pfind-small.txt>txt/pfind-small.txt</a></b> - Five phone numbers from <b>txt/phones-small.txt</b> and five random phone numbers.
<LI> <b><a href=txt/pfind-big.txt>txt/pfind-big.txt</a></b> - 50,000 phone numbers from <b>txt/phones-big.txt</b> and 50,000 random phone numbers.
</UL>

Below, I'll run the two programs on the small files:

<pre>
UNIX> <font color=darkred><b>bin/store_find_set txt/phones-small.txt txt/pfind-small.txt Y</b></font>
Data:
009-759-6084        <font color=blue> # The only difference in the two outputs is that this is sorted.</font>
062-707-0682
161-804-8876
276-780-5793
366-672-5281
392-698-1589
639-049-9982
874-615-3750
927-211-9485
943-433-6132

067-449-4119: Not found
634-692-2465: Not found
087-310-7338: Not found
062-707-0682: Found
750-158-1494: Not found
927-211-9485: Found
639-049-9982: Found
366-672-5281: Found
158-103-1526: Not found
276-780-5793: Found

Found 5
UNIX> <font color=darkred><b>bin/store_find_unordered txt/phones-small.txt txt/pfind-small.txt Y</b></font>
Data:
927-211-9485        <font color=blue> # And this is not sorted.</font>
639-049-9982
366-672-5281
161-804-8876
943-433-6132
276-780-5793
009-759-6084
062-707-0682
874-615-3750
392-698-1589

067-449-4119: Not found
634-692-2465: Not found
087-310-7338: Not found
062-707-0682: Found
750-158-1494: Not found
927-211-9485: Found
639-049-9982: Found
366-672-5281: Found
158-103-1526: Not found
276-780-5793: Found

Found 5
UNIX> <font color=darkred><b></b></font>
</pre>

Now, let's time them on the big files:

<pre>
UNIX> <font color=darkred><b>time bin/store_find_set txt/phones-big.txt txt/pfind-big.txt N</b></font>
Found 50000

real	0m0.522s
user	0m0.516s
sys	0m0.005s
UNIX> <font color=darkred><b>time bin/store_find_unordered txt/phones-big.txt txt/pfind-big.txt N</b></font>
Found 50000

real	0m0.180s
user	0m0.174s
sys	0m0.005s
UNIX> <font color=darkred><b></b></font>
</pre>

The difference is significant!  Even though <i>O(log n)</i> is pretty small (in this example,
it is 17), the difference is enough to make the programs run at significantly different
speeds.  The comparison is not as strong as it should be, because in both programs, reading
the files takes a lot of time.  
<p>
Regardless, I hope this is convincing enough to you to pay attention to these data structures
and use them when you don't need sorting.

<hr>
<H3>Is there an unsorted_multiset?</h3>

Yes, and <tt>unsorted_multimap</tt>.
<hr>
<H3>Bottom Line</h3>

I know I'm repeating myself -- if you need storage and retrieval, and you don't need
for your data to be sorted, you will gain significant speedups if you use
<tt>unsorted_set</tt> and <tt>unsorted_map</tt> instead of 
<tt>set</tt> and <tt>map</tt>.
