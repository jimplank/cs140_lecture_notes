/* This is the exact same as store_find_set.cpp, except we are using an
   unordered_set rather than a set. */

#include <unordered_set>
#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char **argv)
{
  ifstream data_file, to_find_file;
  bool print;
  unordered_set <string> data;
  unordered_set <string>::const_iterator f;
  string s;
  int found;

  /* Parse the command line. */

  try {
    if (argc != 4) throw (string) "usage: store_find_unordered data_file to_find_file print(Y/N)\n";
    data_file.open(argv[1]);
    if (data_file.fail()) throw (string) "can't open " + argv[1];
    to_find_file.open(argv[2]);
    if (to_find_file.fail()) throw (string) "can't open " + argv[2];
    print = (argv[3][0] == 'Y');
  } catch (const string &s) {
    cerr << s << endl;
    return 1;
  }
 
  /* Read the data file. */

  while (data_file >> s) data.insert(s);

  if (print) {
    cout << "Data:" << endl;
    for (f = data.begin(); f != data.end(); f++) cout << *f << endl;
    cout << endl;
  }
 
  data_file.close();

  /* Read the to_find_file, and try to find each word in the data file */

  found = 0;

  while (to_find_file >> s) {
    f = data.find(s);
    if (f != data.end()) found++;
    if (print) cout << s << ": " << ((f == data.end()) ? "Not found" : "Found") << endl;
  }

  if (print) cout << endl;
  cout << "Found " << found << endl;
  return 0;
}
