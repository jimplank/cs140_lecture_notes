#include <iostream>
using namespace std;

class B {
  public:
   B(const B &x) {
      cout << "B's copy constructor\n";
   }
   B() {
      cout << "B's 0-arg constructor\n";
   }
   const B &operator=(const B &x) {
      cout << "B's assignment operator\n";
      return *this;
   }
};

class C {
  public:
   C(const C &x) {
      cout << "C's copy constructor\n";
   }
   C() {
      cout << "C's 0-arg constructor\n";
   }
   const C &operator=(const C &x) {
      cout << "C's assignment operator\n";
      return *this;
   }
};

// class A shows why it's better to use an initialization statement
// than an assignment statement to initialize class variables
class A {
   protected:
      B bValue;
      C cValue;
   public:
      A(B &x, C &y) : cValue(y) {
         bValue = x;
      }
};

int main() {
   B b;
   C c;
   cout << "\nnow declaring A\n";
   A a(b, c);
   int i;

   cout << "\nEntering the for loop\n";
   // x will be allocated and de-alloated each time through the loop
   for (i = 0; i < 3; i++) {
      B x;
   }
}

