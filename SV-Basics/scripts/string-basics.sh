i=0
while [ $i -lt 7 ]; do
  clear
  echo "Basic string functionalities:"
  echo ""

  if [ $i -ge 1 ]; then echo 'Assigning them from string literals.'; fi
  if [ $i -ge 2 ]; then echo 'Using size() to determine their size.'; fi
  if [ $i -ge 3 ]; then echo 'Changing their contents by treating them like an array.'; fi
  if [ $i -ge 4 ]; then echo 'Testing equality using "=="'; fi
  if [ $i -ge 5 ]; then echo 'Comparing them using "<", etc.'; fi
  if [ $i -ge 6 ]; then echo 'Concatenating them with "+".'; fi
  i=$(($i+1))
  read a
done
