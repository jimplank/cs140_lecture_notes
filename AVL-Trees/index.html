
<title>CS202 Lecture Notes - AVL Trees</title>
<h2>CS202 Lecture Notes - AVL Trees</h2>
<UL>
<LI> James S. Plank
<LI> Directory: <b>/home/plank/cs202/Notes/AVLTree</b>
<LI> Original Notes: <i> Mid 2000's.</i>
<LI> Last Revision: <i>
Mon Nov  8 13:00:09 EST 2021
</i>
</UL>

AVL Trees are binary search trees that are <i>balanced</i>.  This is a 
very nice property because it guarantees that finds, insertions and
deletions are all executed in logarithmic time.
That means that if there are <i>n</i> items in the tree, the operations
take roughly <i>log<sub>2</sub>n</i> time.  That's as good as you can do,
by the way.

To help motivate the need for balancing, think about what happens if you
insert into a regular binary search tree, and the input is already 
sorted.  The tree becomes a gigantic line, and if the size of the tree
is <i>n</i> elements, then performing each insertion
takes <i>n</i> operations.  Performing all <i>n</i> insertions takes
roughly <i>n<sup>2</sup></i> operations.  For example, go into the lecture
note directory for binary search trees, and take a look at the file 
txt/input.txt
in the binary search tree directory):
<p>

<pre>
UNIX> <font color=darkred><b>wc txt/input.txt</b></font>      <font color=blue> # This is in the Trees lecture note directory</font>
   50000  200000 2397902 txt/input.txt
UNIX> <font color=darkred><b>head txt/input.txt</b></font>
INSERT Brooke-Footwork 443-90-4990 898-934-4865
INSERT Sophia-Allison-Bromfield 510-30-7699 873-553-7759
INSERT Grace-Barnabas 948-49-5092 562-672-8825
INSERT Cole-Illogic 225-22-1798 976-177-7104
INSERT Elizabeth-Green 451-59-3245 106-637-5581
INSERT Dylan-Bambi 183-22-7881 033-896-1807
INSERT Anna-Hitch 284-19-1258 072-144-3834
INSERT Michael-Bilayer 741-13-7226 327-981-7902
INSERT Gavin-Harriman 831-80-7194 488-419-0189
INSERT Charlie-Iii 998-93-7448 930-447-4165
UNIX> <font color=darkred><b>time cat txt/input.txt | bin/bstree_tester -</b></font>

real	0m0.435s
user	0m0.424s
sys	0m0.007s
UNIX> <font color=darkred><b>time sort txt/input.txt | head -n 1000 | bin/bstree_tester -</b></font>

real	0m0.512s
user	0m0.506s
sys	0m0.010s
UNIX> <font color=darkred><b>time sort txt/input.txt | head -n 2000 | bin/bstree_tester -</b></font>

real	0m0.633s
user	0m0.629s
sys	0m0.010s
UNIX> <font color=darkred><b>time sort txt/input.txt | head -n 4000 | bin/bstree_tester -</b></font>

real	0m1.273s
user	0m1.269s
sys	0m0.010s
UNIX> <font color=darkred><b>time sort txt/input.txt | head -n 8000 | bin/bstree_tester -</b></font>

real	0m3.747s
user	0m3.741s
sys	0m0.015s
UNIX> <font color=darkred><b>time sort txt/input.txt | bin/bstree_tester -</b></font>

real	2m7.399s
user	2m7.181s
sys	0m0.140s
UNIX> <font color=darkred><b></b></font>
</pre>


That's a big problem with binary search trees.  AVL trees (and other balanced
trees like Splay trees, Red-Black trees, B-trees, 2-3 trees, etc) make sure
that their trees are balanced so that the various operations are much faster.
For example, the program <b>avltree_test</b> is my solution to the AVL Tree
lab (which some semesters will not have the pleasure of implementing):

<pre>
UNIX> <font color=darkred><b>time sort txt/input.txt > /dev/null</b></font>

real	0m0.436s
user	0m0.428s
sys	0m0.007s
UNIX> <font color=darkred><b>time cat txt/input.txt | bin/avltree_tester -</b></font>

real	0m0.296s
user	0m0.291s
sys	0m0.007s
UNIX> <font color=darkred><b>time sort txt/input.txt | bin/avltree_tester -</b></font>

real	0m0.698s
user	0m0.721s
sys	0m0.012s
UNIX> <font color=darkred><b></b></font>
</pre>


As you can see, since sorting takes .43 seconds, performing insertions with 
the AVL tree takes the same time when the input is sorted as when it is not
sorted.

<hr>
<h3>Rotations</h3>
<p>
A central operation with AVL Trees is a <i>rotation</i>.  It is a way of 
changing a binary search tree so that it remains a binary search tree,
but changes how it is balanced.  The concept is illustrated below:

<p><center><table border=0><td><img src=jpg/Balance.jpg width=450></td></table></center><p>

<b>B</b> and <b>D</b> are nodes in a binary search tree.  They can occur anywhere
a tree, but we don't worry about what's above them -- just what's below them.
<b>A</b>, <b>C</b> and <b>E</b> are subtrees that rooted as the children of
<b>B</b> and <b>D</b>.  They may be empty.  If they are not empty, then since
the tree is a binary search tree, we know that:
<p>
<UL>
<LI> Every node in <b>A</b> has a key less than <b>B</b>'s key.
<LI> Every node in <b>C</b> has a key less than <b>D</b>'s key, but greater than <b>B</b>'s key.
<LI> Every node in <b>E</b> has a key greater than <b>D</b>'s key.
</UL>
<p>
When we perform a rotation, we perform it <i>about</i> a node.  For example,
the rotation pictured above rotates about node <b>D</b> to turn the tree on the
left to the tree on the right.  It also shows that you can turn the tree on the
right to the tree on the left by rotating about node <b>B</b>.
<p>
When you rotate about a node, you are going to change the tree so that the
node's parent is now the node's child.  The middle subtree (subtree <b>C</b>) 
will change from being one node's child to being the other node's child.
The rotation does not violate any of the properties of binary search trees.
However, it changes the shape of the tree, and there are multiple types
of trees, like AVL, Splay  and Red-Black trees that 
employ rotations to ensure that the trees are balanced.
<p>
Below are some example rotations. Make sure you understand all of them:

<p><center><table border=3><tr><td><img src=jpg/B1.jpg width=450></td></tr>
                           <tr><td><img src=jpg/B2.jpg width=500></td></tr>
                           <tr><td><img src=jpg/B3.jpg width=500></td></tr>
                           <tr><td><img src=jpg/B4.jpg width=500></td></tr></table></center><p>


<hr>
<h3>AVL Trees</h3>

An AVL Tree is a binary search tree that has conditions on the <i>height</i> of each 
node.  The height is defined to be the number of nodes in the longest path from that node
to a leaf node.  Thus, in the left tree in the last diagram, <i>Eunice's</i>
height is four and <i>Binky's</i> height is three.  In the right tree, 
<i>Eunice's</i> 
height is still four, but <i>Binky's</i> height is now two.  Leaf nodes have
a height of one.  Since each node roots a subtree, we say that the height of 
a subtree is the height of its root. An empty tree has a height of 0.
<p>
The definition of an AVL tree is follows:
<p>
<UL> 
<LI> Every subtree of the tree is an AVL tree.
<LI> For each node, the difference between the heights of its left and right subtrees is 
at most one.
</UL>
That's a pretty simple definition.  However, the constraint on the heights of each node's
children is what makes the trees balanced, and makes insertion, finding, and deletion
<i>O(log n)</i>.
<p>

Below are some AVL trees:

<p><center><table border=3><td><img src=jpg/AVL-1.jpg width=40 valign=center align=center></td>
<td><img src=jpg/AVL-2.jpg width=80 valign=center align=center></td>
<td><img src=jpg/AVL-3.jpg width=120 valign=center align=center></td>
<td><img src=jpg/AVL-4.jpg width=160 valign=center align=center></td>
<td><img src=jpg/AVL-5.jpg width=180 valign=center align=center></td></table></center><p>

And below are two trees that are binary search trees, but are not AVL
trees.
<p><center><table border=3><td valign=bottom align=center><img src=jpg/Non-AVL-1.jpg width=80>
<br><i>Binky</i> violates the definition</td>
<td valign=bottom align=center><img src=jpg/Non-AVL-2.jpg width=180>
<br><i>Fred</i> violates the definition</td></table></center><p>

<hr>
<h3>Insertion into AVL Trees</h3>

To implement AVL trees, you need to maintain the height of each node.
You insert into an AVL tree by performing a standard binary tree insertion.  
When you're done, you check each node on the path from the new node to
the root.  The checking goes as follows:

<UL>
<LI> If that node's height hasn't changed because of the insertion,
then you are done.  You may stop checking, and the insertion is complete.
<LI> If the node's height has changed, but it does not
violate the balance property, then you continue checking the next node
in the path up to the root.
<LI>  If the node's height has changed and it now violates the
balance property, then you need to perform one or two rotations to 
fix the problem, and then you are done.
</UL>
<p>
Let's try some examples.  Suppose I have the following AVL tree -- I now annotate
the nodes with their heights:

<p><center><table border=0><td><img src=jpg/AVL-6.jpg width=200></td></table></center><p>

If I insert <i>Ahmad</i>, take a look at the resulting tree:

<p><center><table border=0><td><img src=jpg/AVL-7.jpg width=200></td></table></center><p>

The new node <i>Ahmad</i> has a height of one, and when I travel the path up to
the root, I change <i>Baby Daisy's</i> height to two.  However, her node is not
imbalanced, since the height of her subtrees are 1 and 0.  Moving on, <i>Binky's</i>
height is unchanged, so we can stop -- the resulting tree is indeed an AVL tree.
<p>
However, suppose I now try to insert <i>Waluigi</i>.  I get the following
tree:

<p><center><table border=0><td><img src=jpg/AVL-8.jpg width=240></td></table></center><p>

Traveling from the new node to the root, I see that <i>Fred</i> violates the balance
condition.  Its left child is an empty tree, and as such has a height of 0. 
Its right child has a height of 2.
I have to <i>rebalance</i> the tree.

<hr>
<h3>Rebalancing</h3>

When you recognize that you have an imbalanced node, it will look like one of the
two pictures below.  This is <i>without exception</i>:

<p><center><table border=3 cellpadding=10>
<td><img src=jpg/Imbalance-B.jpg></td>
<td><img src=jpg/Imbalance-D.jpg></td>
</table></center><p>

Up to two of the three subtrees <i>A</i>, <i>C</i> and <i>E</i> may be empty in this picture, 
but all three won't be empty.   You'll note that the two pictures are mirror copies
of one another.

<h3>Zig-Zig</h3>

<p>
Now, each of these may be further broken up into two cases, which we call "Zig-Zig"
and "Zig-Zag".  Let's concentrate on Zig-Zig, because it is simpler.  Here is what
it looks like in its two mirror images:

<p><center><table border=3 cellpadding=10>
<td><img src=jpg/Imbalance-Zig-B.jpg></td>
<td><img src=jpg/Imbalance-Zig-D.jpg></td>
</table></center><p>

You'll note that the defining feature is that the direction of the imbalance either
goes from the right child of the root to its right child (in the left picture), 
or from the left child of the root to its left child (in the right picture).
That's why it's called "Zig-Zig".
<p>
To fix the Zig-Zig imbalance, you rotate about the child.  That "fixes" the imbalance
in each case, and it also decreases the height of the tree.  In the pictures below,
make sure that you double-check all of the nodes to make sure they meet their balance
conditions:

<p><center><table border=3 cellpadding=10>
<td align=center><img src=jpg/Rebalance-Zig-B.jpg><br>
This is the left tree above, rotated about node D.  </td>
<td align=center><img src=jpg/Rebalance-Zig-D.jpg><br>
This is the right tree above, rotated about node B.  </td>
</table></center><p>

It's now a good time to do some examples where we insert a node into an AVL tree,
it becomes imbalanced due to a Zig-Zig imbalance, and we then fix it with a rotation.
What I'm going to do in each picture below is show the tree and state what node
we're inserting.  Then I'll draw the resulting tree, which is imbalanced, and I'll
shade the 
<i>A</i>, <i>C</i> and <i>E</i> subtrees.  I'll then show the balanced tree that
results when you perform the rotation:

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/AVL-Add-Ralph-1.jpg width=260><p>
We insert "Ralph"
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Ralph-2.jpg width=260><p>
"Khloe" is imbalanced.<br>
We rotate about "Luther"
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Ralph-3.jpg width=260><p>
It's an AVL Tree again!
</td>
</table></center>

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/AVL-Add-Becca-1.jpg width=260><p>
We insert "Becca"
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Becca-2.jpg width=280><p>
"Eunice" is imbalanced.<br>
We rotate about "Cal"
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Becca-3.jpg width=300><p>
It's an AVL Tree again!
</td>
</table></center>


<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/AVL-Add-Zelda-1.jpg><p>
We insert "Zelda"<br>
Sometimes this example is confusing.
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Zelda-2.jpg><p>
"Henry" is imbalanced.<br>
We rotate about "Mike"
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Zelda-3.jpg><p>
It's an AVL Tree again!
</td>
</table></center>

<hr>

One important thing to note is that after the rebalancing, the entire AVL tree
is balanced -- the height of the rebalanced subtree is the same after rebalancing
than it was before the node was inserted.  

<h3>Zig-Zag</h3>

<p>
The "Zig-Zag" imbalance happens when the imbalance goes right, then left, or
left, then right.  Here's what it looks like in its two
mirror images:

<p><center><table border=3 cellpadding=10>
<td><img src=jpg/Imbalance-Zag-B.jpg></td>
<td><img src=jpg/Imbalance-Zag-D.jpg></td>
</table></center><p>

To explain how to fix this, we need to blow up the <i>C</i> tree in the picture
above, relabeling the nodes and subtrees so that they make sense:

<p><center><table border=3 cellpadding=10>
<td><img src=jpg/Imbalance-Zag-More-B.jpg></td>
<td><img src=jpg/Imbalance-Zag-More-F.jpg></td>
</table></center><p>

To rebalance the Zig-Zag case, we need to rotate twice about the grandchild.
In each of these pictures, the grandchild is <i>D</i>.  In the pictures
below, we rotate once about <i>D</i>, but the tree is not balanced yet:

<p><center><table border=3 cellpadding=10>
<td align=center><img src=jpg/Imbalance-Zag-Once-B.jpg><p>
One rotation about <i>D</i>:<br>
The tree is still imbalanced.</td>
<td align=center><img src=jpg/Imbalance-Zag-Once-F.jpg><p>
One rotation about <i>D</i>:<br>
The tree is still imbalanced.</td>
</table></center><p>

We perform one more rotation about <i>D</i>, and now the tree is balanced.
In fact, in both cases, the resulting trees are identical!

<p><center><table border=3 cellpadding=10>
<td align=center><img src=jpg/Imbalance-Zag-Done.jpg><p>
After the second rotation about <i>D</i>:<br>
The tree is balanced!</td>
<td align=center><img src=jpg/Imbalance-Zag-Done.jpg><p>
After the second rotation about <i>D</i>:<br>
The tree is balanced!</td>
</table></center><p>

<p>
Let's do some examples:

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/AVL-Add-Don-1.jpg width=260><p>
We insert "Don".
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Don-2.jpg width=280><p>
"Eve" is imbalanced.<br>
We rotate twice about "Cal".
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Don-3.jpg width=300><p>
It's an AVL Tree again!
</td>
</table></center>


<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/AVL-Add-Eve-1.jpg width=260><p>
We insert "Eve".
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Eve-2.jpg width=280><p>
"Kim" is imbalanced.<br>
We rotate twice about "Ian".
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Eve-3.jpg width=300><p>
It's an AVL Tree again!
</td>
</table></center>


<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/AVL-Add-Ginger-1.jpg width=150><p>
We insert "Ginger".
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Ginger-2.jpg width=270><p>
"Brad" is imbalanced.<br>
We rotate twice about "Ginger".
</td>
<td valign=top align=center>
<img src=jpg/AVL-Add-Ginger-3.jpg width=150><p>
It's an AVL Tree again!
</td>
</table></center>

As with Zig-Zig, after rebalancing, the height of the rebalanced subtree is the same
as it was before the node was inserted. 

<hr>
<h3>Summary of Insertion</h3>

<UL>
<LI> You insert a node into an AVL tree just like you insert a node into a regular
binary search tree.
<LI> After inserting, you need to travel from this new node up to the root, and
potentially increment the height of the nodes.
<LI> You also need to check to see if a node is imbalanced.  If it is, you need to
rebalance.
<LI> To rebalance, you need to identify whether it is a Zig-Zig or a Zig-Zag imbalance.
<LI> If Zig-Zig, then you rotate once about the child of the imbalanced node.
<LI> If Zig-Zag, then you rotate twice about the grandchild of the imbalanced node.
<LI> After rebalancing, you are done.
</UL>

<hr>
<h3>Deletion</h3>

When you delete a node, then you need to start at the parent, and travel up to the
root.  For each node that you encounter, you need to do the following:
<UL>
<LI> Check to see if node is balanced.  If it is, you may need to decrement its
height.
<LI> If it is imbalanced, then you need to identify whether it is Zig-Zig or Zig-Zag.
<LI> You treat Zig-Zig and Zig-Zag as described above, performing one or two rotations
     to rebalance the tree.
</UL>

With deletion, you can get the following imabalanced trees (an of course their
mirror images).  The first and third ones
are the same as with insertion.  The middle one can only occur with deletion.

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/Delete-Imbalance-1.jpg width=200><p>
Zig-Zig -- same as above.
</td>
<td valign=top align=center>
<img src=jpg/Delete-Imbalance-2.jpg width=200><p>
This case only occurs with deletion.  We
treat it as a Zig-Zig.
</td>
<td valign=top align=center>
<img src=jpg/Delete-Imbalance-3.jpg width=200><p>
Zig-Zag -- same as above.
</td>
</table></center>
<p>

After rebalancing, you can't stop, as you do with insertion.  Instead you need to
keep traveling toward the root.  You only stop when you reach the root, or you
don't change the height of a node (because then the heights of its ancestor nodes
won't change either).

<p>
Let's look at some examples.  As with insertion, I'll show an original tree
before deletion, the tree after deletion, but before rebalancing, 
and the tree after rebalancing.

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/Delete-Hal-1.jpg width=400><p>
We delete "Hal".
</td>
<td valign=top align=center>
<img src=jpg/Delete-Hal-2.jpg width=400><p>
We check Hal's parent, Ian, and it's balanced
and its height is unchanged.  We're done.
</table></center>

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/Delete-Ian-1.jpg width=350><p>
We delete "Ian".
</td>
<td valign=top align=center>
<img src=jpg/Delete-Ian-2.jpg width=350><p>
As we move up to the root, we see that "Cal"
is imbalanced.  It's a Zig-Zig, so we 
rotate about "Bob".
</td>
<td valign=top align=center>
<img src=jpg/Delete-Ian-3.jpg width=350><p>
It's an AVL Tree again.  You'll note we still
had to travel up from Bob to the root, and
change Kim's height from 5 ato 4.  
</td>
</table></center>
<p>

<p><center><table border=3 cellpadding=10>
<td valign=top align=center>
<img src=jpg/Delete-Nell-1.jpg width=250><p>
We delete "Nell".  To do that, we find the largest
node in Nell's left subtree -- Anne.  We delete
Anne and replace Nell with Anne.
</td>
<td valign=top align=center>
<img src=jpg/Delete-Nell-2.jpg width=150><p>
Anne is imbalanced.
It's a Zig-Zag, so we 
rotate twice about "Omar".
</td>
<td valign=top align=center>
<img src=jpg/Delete-Nell-3.jpg width=150><p>
It's an AVL Tree again. 
</td>
</table></center>
<p>

<p><center><table border=3 cellpadding=10>
<tr><td valign=top align=center>
<img src=jpg/Delete-Naomi-1.jpg width=450><p>
We delete "Naomi".  To do that, we find the largest
node in Naomi's left subtree -- Jet.  We delete
Jet and replace Naomi with Jet.
</td>
<td valign=top align=center>
<img src=jpg/Delete-Naomi-2.jpg width=450><p>
Jet is imbalanced.
It's a Zig-Zig, so we 
rotate about "Samson".
</td></tr>
<tr>
<td valign=top align=center>
<img src=jpg/Delete-Naomi-3.jpg width=450><p>
That subtree is balanced, but have to continue going to the root.
We find that "Henry" is also imbalanced, and that it's a 
Zig-Zag.  Two rotations about ""Dub".
</td>
<td valign=top align=center>
<img src=jpg/Delete-Naomi-4.jpg width=450><p>
Now, we're done and the tree is balanced.
</td>
</table></center>
<p>

<hr>
<h3>Running Times</h3>

The balance condition means that AVL trees' heights are <i>O(log n)</i>.  Therefore:
<UL>
<LI> Inserting into an AVL tree with <i>n</i> nodes: <i>O(log n)</i>.
<LI> Finding an item in an AVL tree with <i>n</i> nodes: <i>O(log n)</i>.
<LI> Deleting from AVL tree with <i>n</i> nodes: <i>O(log n)</i>.
<LI> Creating an AVL tree that has <i>n</i> nodes: <i>O(n log n)</i>.
<LI> Performing any of those recursive traversals on an AVL tree with <i>n</i> nodes: <i>O(n)</i>.
</UL>
