/* This program sorts integers read on standard 
   input, using the sort() procedure from the 
   STL algorithms library. */

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main()
{
  vector <int> numbers;
  int n;
  size_t i;

  /* Read integers into the vector "numbers".
     Sort the vector using the sort() procedure, 
     and print the sorted numbers. */

  while (cin >> n) numbers.push_back(n);
  sort(numbers.begin(), numbers.end());
  for (i = 0; i < numbers.size(); i++) {
    cout << numbers[i] << endl;
  }

  return 0;
}
