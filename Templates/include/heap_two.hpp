#pragma once
#include <vector>
#include <string>

template <typename Key, typename Val>
class Heap {
  public:
    void    Push(const Key &k, const Val &v);
    Val     Pop();
    size_t  Size() const;
    bool    Empty() const;

  protected:
    std::vector < std::pair<Key, Val > > h;
};

template <typename Key, typename Val>
size_t Heap<Key,Val>::Size() const  { return h.size(); }

template <typename Key, typename Val>
bool   Heap<Key,Val>::Empty() const { return h.empty(); }

template <typename Key, typename Val>
void Heap<Key,Val>::Push(const Key &k, const Val &v)
{ 
  int i, parent;
  std::pair<Key,Val> tmp;

  h.push_back(std::make_pair(k, v));

  i = h.size()-1;
  while (1) {
    if (i == 0) return;
    parent = (i-1)/2;
    if (h[parent].first > h[i].first) {
      tmp = h[i];
      h[i] = h[parent];
      h[parent] = tmp;
      i = parent;
    } else {
      return;
    }
  }
}

template <typename Key, typename Val>
Val Heap<Key,Val>::Pop() 
{
  Val retval;
  std::pair<Key,Val> tmp;
  size_t lc, rc;
  int index;
  
  if (h.empty()) throw (std::string) "Called Pop() on an empty heap";

  retval = h[0].second;
  h[0] = h[h.size()-1];
  h.pop_back();
  if (h.size() == 0) return retval;

  index = 0;

  while (1) {                
    lc = index*2+1;
    rc = lc+1;
   
    if (lc >= h.size()) return retval;

    if (rc == h.size() || h[lc].first <= h[rc].first) {
      if (h[lc] < h[index]) {
        tmp = h[lc];
        h[lc] = h[index];
        h[index] = tmp;
        index = lc;
      } else {
        return retval;
      }

    } else if (h[rc] < h[index]) {
      tmp = h[rc];
      h[rc] = h[index];
      h[index] = tmp;
      index = rc;
    } else {
      return retval;
    }
  }
}
