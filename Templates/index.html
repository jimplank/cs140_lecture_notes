<title>CS202 Lecture Notes - Templates</title>
<h2>CS202 Lecture Notes - Templates</h2>
<UL>
<LI> James S. Plank
<LI> Directory: <b>/home/jplank/cs202/Notes/Templates</b>
<LI> Original notes: 2021.
<LI> Last Revision: <i>
Wed May  3 18:07:36 EDT 2023
</i>
</UL>

<h3>Constructs That Make Life "Easier"</h3>

As you start programming bigger and better things, you will find parts of your
programming language to be constricting.  Depending on the language, there will be
some constructs that are defined to make life less constricting. The following
constructs in C and C++ fit this description:

<UL>
<LI> Unions (C and C++)
<LI> (void *) (C and C++)
<LI> Polymorphism (C++)
<LI> Inheritance (C++)
<LI> Operator overloading (C++)
<LI> Templates (C++)
<LI> Auto variables (C++)
</UL>

All of these have similar plusses and minuses -- the plusses are generality, flexibility
and sometimes efficiency.  The major minus is readability, for all of these, and sometimes
they handcuff your code structure and you end up making bad decisions.
For that reason I always caution students to think carefully before launching into any
of these.  
<p>
To highlight this, I worked with a colleague who was an excellent programmer, and who decided
that templates would add flexibility to our project.  At first, it was great, but as the
software developed, the template required us to duplicate code, which led to bugs.  As it turns
out, interfaces were the far preferable solution, and we ended up ripping out the templates
and reimplementing.  That's part of life, but it is also a cautionary tale.  Think twice.
<p>
On the flip side, the Standard Template Library is a beautiful use of templates that most
definitely improves functionality, readbility and reusability.
<p>
So let's learn.
<hr>
<h3>The Basics</h3>

Templates allow you to define a procedure, class or method that employs data whose type is
flexible.  Sometimes it will work with any type, and sometimes it will work with specific
classes of types.  Let's show the latter.  This code comes by way of Dr. Gregor, BTW.
In the program <b><a href=src/add_flex.cpp>src/add_flex.cpp</a></b>, I define a procedure
<b>add()</b> which uses a template for its parameters and its return value:

<p><center><table border=3 cellpadding=3><td><pre>
template &lt;typename T&gt;
T add(const T &v1, const T &v2)        <font color=blue>// This code returns the "sum" of its arguments,</font>
{                                      <font color=blue>// and it works on any type where '+' is defined.</font>
  return v1 + v2; 
}
</pre></td></table></center><p>

The "template" line says that the procedure may use the type named <b>T</b> as a placeholder
for whatever type will be used.  So long as the type being used has the '+' operator
defined, you may call <b>add()</b> on instances of the type.
<p>
Here's a <b>main()</b> that calls <b>add()</b> three times -- once on integers, once
on doubles and once on strings:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Our main will first read two integers and print their sum,
   then two doubles and print their sum,
   then two strings and print their sum. */</font>

int main() 
{
  int i, j;
  double x, y;
  string s, t;

  cout &lt;&lt; "Enter two integers: " ;
  cin &gt;&gt; i &gt;&gt; j;
  cout &lt;&lt; "Their sum is " &lt;&lt; add(i, j) &lt;&lt; endl &lt;&lt; endl;

  cout &lt;&lt; "Enter two doubles: " ;
  cin &gt;&gt; x &gt;&gt; y;
  cout &lt;&lt; "Their sum is " &lt;&lt; add(x, y) &lt;&lt; endl &lt;&lt; endl;

  cout &lt;&lt; "Enter two strings: " ;
  cin &gt;&gt; s &gt;&gt; t;
  cout &lt;&lt; "Their sum is " &lt;&lt; add(s, t) &lt;&lt; endl &lt;&lt; endl;

  return 0;
}
</pre></td></table></center><p>

We run it, and as you can see, it has added quite a bit of flexibility to our code --
the proper '+' is called for each data type:

<pre>
UNIX> <font color=darkred><b>bin/add_flex</b></font>
Enter two integers: <font color=darkred><b>4 9</b></font>
Their sum is 13                 <font color=blue> # Here, add() is called on integers.</font>

Enter two doubles: <font color=darkred><b>3.4 5.5</b></font>
Their sum is 8.9                <font color=blue> # Here, add() is called on doubles.</font>

Enter two strings: <font color=darkred><b>Jim Plank</b></font>
Their sum is JimPlank           <font color=blue> # Here, add() is called on strings.</font>

UNIX> <font color=darkred><b></b></font>
</pre>

If you want to treat a type in a special manner, you can do so --
in <b><a href=src/add_flex_s.cpp>src/add_flex_s.cpp</a></b>, we add a special procedure
for strings that puts a space in the middle.  

<p><center><table border=3 cellpadding=3><td><pre>
template &lt;typename T&gt;
T add(const T &v1, const T &v2)        <font color=blue>// Same code as before.</font>
{                                      
  return v1 + v2; 
}

                                       <font color=blue>// We add this code to treat strings differently</font>
                                       <font color=blue>// from other types:</font>
template &lt;&gt;
string add(const string &v1, const string &v2) 
{ 
  return v1 + " " + v2; 
}
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>bin/add_flex_s</b></font>
Enter two integers: <font color=darkred><b>4 9</b></font>
Their sum is 13

Enter two doubles: <font color=darkred><b>3.4 5.5</b></font>
Their sum is 8.9

Enter two strings: <font color=darkred><b>Jim Plank</b></font>     <font color=blue> # Here the special code is called for strings.</font>
Their sum is Jim Plank

UNIX> <font color=darkred><b></b></font>
</pre>

I'll be honest -- I don't use templates, but I can see how their usage above can simplify
code that does the same thing, but on different numerical types.

<hr>
<h3>Using templates to hold generic data</h3>

The standard template library uses templates as a way to hold generic data.
This is why, for example, vectors, deques and lists can hold anything, and sets/maps
can work on any data type that defines comparison.
<p>
Let's show an example of how one might implement a more generic data structure with
templates.  In
<b><a href=include/heap_one.hpp>include/heap_one.hpp</a></b>, I define (and implement)
a <b>Heap</b> data structure that uses a template so that it can store any data type
where '<'  and '>' are defined:

<p><center><table border=3 cellpadding=3><td><pre>
#pragma once
#include &lt;vector&gt;

template &lt;typename T&gt;
class Heap {
  public:
    void    Push(const T &amp;d);
    T       Pop();
    size_t  Size() const;
    bool    Empty() const;

  protected:
    std::vector &lt;T&gt; h;
};
</pre></td></table></center><p>

I have two programs that use this data structure to print the 5 minimum elements
on standard input.  One prints integers, and the other prints strings:

<p><center><table border=3 cellpadding=3><td><pre>
<b><a href=src/h1_min_5_int.cpp>src/h1_min_5_int.cpp</a></b>

#include &lt;iostream&gt;
#include "heap_one.hpp"
using namespace std;

<font color=blue>/* This uses the templated Heap data structure 
   from heap_one.hpp to print the
   five smallest integers on standard input. */</font>

int main()
{
  Heap &lt;int&gt; h;
  int i;

  while (cin &gt;&gt; i) h.Push(i);

  for (i = 0; i &lt; 5 && !h.Empty(); i++) {
    cout &lt;&lt; h.Pop() &lt;&lt; endl;
  }
  return 0;
}
</pre></td>
<td valign=top><pre>
<b><a href=src/h1_min_5_str.cpp>src/h1_min_5_str.cpp</a></b>

#include &lt;iostream&gt;
#include "heap_one.hpp"
using namespace std;

<font color=blue>/* This uses the templated Heap data structure 
   from heap_one.hpp to print the
   five smallest strings on standard input. */</font>

int main()
{
  Heap &lt;string&gt; h;
  string s;
  int i;

  while (getline(cin, s)) h.Push(s);

  for (i = 0; i &lt; 5 && !h.Empty(); i++) {
    cout &lt;&lt; h.Pop() &lt;&lt; endl;
  }
  return 0;
}
</pre></td></table></center><p>

That's really convenient, isn't it?  One unfortunate thing, however, is that I have to
implement the data structure in the header file.  The reason has to do with compilation.
I won't go into it -- if you want all of the pedantry and condescension that you can 
handle, read about it on stack overflow.
<p>
Here, for example, is how <b>Size()</b> is implemented -- you have to define the template,
and put the template into the class specification:

<p><center><table border=3 cellpadding=3><td><pre>
template &lt;typename T&gt;
size_t Heap&lt;T&gt;::Size() const  { return h.size(); }
</pre></td></table></center><p>

As always, I put the implementation separate from the class defintion to make
the definition clean and easy to read.

<hr>
<h3>Making the heap data structure more useful</h3>

The <b>Heap</b> implemented above shares a problem with the <b>priority_queue</b>
data structure of the standard template library.  If you want to store an instance of
a class, you have to define the '<'/'>' operators on the class.  (In the STL, you can also
pass a comparison function).
<p>
Let's change our heap so that it holds a key and a value, like maps and multimaps do.
We'll do it a little differently -- when you <b>Push()</b> you specify and key and
a val, and when you <b>Pop()</b> you get the val that corresponds to the minimum key.
Here's the definition of the class, 
in
<b><a href=include/heap_two.hpp>include/heap_two.hpp</a></b>

<p><center><table border=3 cellpadding=3><td><pre>
#pragma once
#include &lt;vector&gt;
#include &lt;string&gt;

template &lt;typename Key, typename Val&gt;
class Heap {
  public:
    void    Push(const Key &k, const Val &v);
    Val     Pop();
    size_t  Size() const;
    bool    Empty() const;

  protected:
    std::vector &lt; std::pair&lt;Key, Val &gt; &gt; h;
};

<font color=blue>/* The methods are implemented below... */</font>

template &lt;typename Key, typename Val&gt;
size_t Heap&lt;Key,Val&gt;::Size() const  { return h.size(); }

<font color=blue>/* You can see the rest in the file. */</font>
</pre></td></table></center><p>

Now, the program
<b><a href=src/h2_min_5_people.cpp>src/h2_min_5_people.cpp</a></b>
reads "people" on standard input, where a person has a first name, last name and
id.  It prints the people with the five minimum id's.
In my opinion, this implementation of the <b>Heap</b> is much more usable.

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;iostream&gt;
#include &lt;cstdio&gt;
#include "heap_two.hpp"
using namespace std;

<font color=blue>/* We're going to read in "people", where a person has a
   first name, a last name, and an id number.  It will
   print the people with the five smallest id numbers. */</font>

class Person
{
  public:
    string fn;
    string ln;
    int id;
};

int main()
{
  Heap &lt;int, Person *&gt; h;
  Person *p;
  int i;
  string fn, ln;

  while (cin &gt;&gt; i &gt;&gt; fn &gt;&gt; ln) {
    p = new Person;
    p-&gt;fn = fn;
    p-&gt;ln = ln;
    p-&gt;id = i;
    h.Push(p-&gt;id, p);
  }

  for (i = 0; i &lt; 5 && !h.Empty(); i++) {
    p = h.Pop();
    printf("%-15s %-15s %8d\n", p-&gt;fn.c_str(), p-&gt;ln.c_str(), p-&gt;id);
  }
  return 0;
}
</pre></td></table></center><p>

Here it is running on 
<b><a href=txt/people.txt>txt/people.txt</a></b>, which contains 10,000 random "people":

<pre>
UNIX> <font color=darkred><b>wc txt/people.txt</b></font>
   10000   30000  229492 txt/people.txt
UNIX> <font color=darkred><b>head txt/people.txt</b></font>
9392853 Zachary Idiotic
6597179 Makayla Walkout
9023575 Jordan Fredrickson
8147031 Jake Snatch
8585074 Charlotte Tray
3524143 Sophia Died
4851306 Jake Sly
2647509 Lucas Rough
4640885 James Babysit
4436205 Ella Barbital
UNIX> <font color=darkred><b>sort -n < txt/people.txt | head -n 5</b></font>
160 Oliver Diameter
571 Sophia Bowie
753 Aiden Pagan
1721 Aaron Cinnamon
3143 Makayla Transient
UNIX> <font color=darkred><b>bin/h2_min_5_people < txt/people.txt</b></font>
Oliver          Diameter             160
Sophia          Bowie                571
Aiden           Pagan                753
Aaron           Cinnamon            1721
Makayla         Transient           3143
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Summary</h3>

Templates allow for greater flexibility in your programs.  At their best, they let you define
data structures that hold arbitrary (or near-arbitrary) data types.  I know I haven't done a 
deep dive into templates here, but I believe I've given you enough to get started with templates
if you are interested.

<p>
I know I'm a broken record here, but you really need to think twice before putting templates
into your code.  While they do allow for flexibility, they also expose you to classes of bugs
that are easy to make and hard to find.
