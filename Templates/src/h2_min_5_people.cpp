#include <iostream>
#include <cstdio>
#include "heap_two.hpp"
using namespace std;

/* We're going to read in "people", where a person has a
   first name, a last name, and an id number.  It will
   print the people with the five smallest id numbers. */

class Person
{
  public:
    string fn;
    string ln;
    int id;
};

int main()
{
  Heap <int, Person *> h;
  Person *p;
  int i;
  string fn, ln;

  while (cin >> i >> fn >> ln) {
    p = new Person;
    p->fn = fn;
    p->ln = ln;
    p->id = i;
    h.Push(p->id, p);
  }

  for (i = 0; i < 5 && !h.Empty(); i++) {
    p = h.Pop();
    printf("%-15s %-15s %8d\n", p->fn.c_str(), p->ln.c_str(), p->id);
  }
  return 0;
}
