#include <iostream>
#include <string>
using namespace std;

template <typename T>
T add(const T &v1, const T &v2)        // This code returns the "sum" of its arguments,
{                                      // and it works on any type where '+' is defined.
  return v1 + v2; 
}

/* Our main will first read two integers and print their sum,
   then two doubles and print their sum,
   then two strings and print their sum. */

int main() 
{
  int i, j;
  double x, y;
  string s, t;

  cout << "Enter two integers: " ;
  cin >> i >> j;
  cout << "Their sum is " << add(i, j) << endl << endl;

  cout << "Enter two doubles: " ;
  cin >> x >> y;
  cout << "Their sum is " << add(x, y) << endl << endl;

  cout << "Enter two strings: " ;
  cin >> s >> t;
  cout << "Their sum is " << add(s, t) << endl << endl;

  return 0;
}
  
