#include <iostream>
#include "heap_one.hpp"
using namespace std;

/* This uses the templated Heap data structure from heap_one.hpp to print the
   five smallest integers on standard input. */

int main()
{
  Heap <int> h;
  int i;

  while (cin >> i) h.Push(i);

  for (i = 0; i < 5 && !h.Empty(); i++) {
    cout << h.Pop() << endl;
  }
  return 0;
}
