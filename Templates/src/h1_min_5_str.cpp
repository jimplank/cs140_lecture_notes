#include <iostream>
#include "heap_one.hpp"
using namespace std;

/* This uses the templated Heap data structure from heap_one.hpp to print the
   five smallest strings on standard input. */

int main()
{
  Heap <string> h;
  string s;
  int i;

  while (getline(cin, s)) h.Push(s);

  for (i = 0; i < 5 && !h.Empty(); i++) {
    cout << h.Pop() << endl;
  }
  return 0;
}
