i=0
while [ $i -le 9 ]; do
  clear
  if [ $i -ge 0 ]; then echo "Here is a summary of the FauxPalindromes Topcoder problem:" ; fi
  if [ $i -ge 1 ]; then echo ""; echo "You are given a string composed of upper-case letters, whose size is between 1 and 50 characters" ; fi
  if [ $i -ge 2 ]; then echo 'If the string is a palindrome (meaning it reads the same backward and forward), the return the string "PALINDROME".' ; fi
  if [ $i -ge 3 ]; then echo 'Otherwise, if the string is a "faux" palindrome (defined below), return the string "FAUX".' ; fi
  if [ $i -ge 4 ]; then echo 'Otherwise, return the string "NOT EVEN FAUX".' ; fi
  if [ $i -ge 5 ]; then echo ""; echo 'A "faux" palindrome is a string where, if you replace all consecutive occurrences'; echo 'of the same letter with a single occurrence of that letter, is a palindrome.' ; echo ""; fi
  if [ $i -ge 6 ]; then echo 'For example, "AABBAAAA" is a faux palindrome.' ; fi
  if [ $i -ge 7 ]; then echo "" ; echo 'Example 0: "ANA"' ; fi
  if [ $i -ge 8 ]; then echo 'Example 1: "AAAAANNAA"' ; fi
  if [ $i -ge 9 ]; then echo 'Example 2: "LEGENDARY"' ; fi
  i=$(($i+1))
  read a
done
