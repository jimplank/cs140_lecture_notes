/* This program demonstrates all of the ways to call the find() method of strings. */

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
using namespace std;

int main()
{
  string a, b;
  size_t i;

  /* Set two strings to use as examples. */

  a = "Lighting Strikes.  Lightning Strikes Again.";
  b = "Light";

  /* Print out the strings with digits over the top, so that it's easier to see the digits. */

  cout << "    ";
  for (i = 0; i < a.size(); i++) cout << i%10;
  cout << endl;

  cout << "a = " << a << endl;
  cout << "b = " << b << endl;
  cout << endl;

  /* We call a.find() in a variety of ways.  Ignore the printf statements, because they
     are a little confusing.  Just look at the calls on the right. */

  printf("a.find(b) = %ld\n",               a.find(b));
  printf("a.find(b, 0) = %ld\n",            a.find(b, 0));
  printf("a.find(b, 1) = %ld\n",            a.find(b, 1));
  printf("a.find(b, 20) = %ld\n",           a.find(b, 20));
  printf("a.find('g') = %ld\n",             a.find('g'));
  printf("a.find('g', 20) = %ld\n",         a.find('g', 20));
  printf("a.find(\"Strike\") = %ld\n",      a.find("Strike"));
  printf("a.find(\"Strike\", 20) = %ld\n",  a.find("Strike", 20));
  printf("a.find(\"Aging\", 0, 2) = %ld\n", a.find("Aging", 0, 2));
  printf("\n");

  /* So, what exactly is string::npos?  If you look up the documentation, it's 
     of type std::string::size_type, which is an unsigned long.  If you use cout
     to print it, you get a giant integer.  As hex, it's 16 f's (all bits set to 
     one).  If you set it or typecast it to a signed long, it becomes -1.  What
     to make of all of this?  Just use "string::npos" when you want to test that
     a find() call has failed, and think smugly to yourself what a poor decision
     it was for find() to return a std::string::size_type... */

  cout << "When you use cout, string::npos is " << string::npos << endl;
  cout << "When you use hex,  string::npos is 0x" << hex << string::npos << endl;
  cout << "Using printf(\"%lu\"), you get:      ";
  printf("%lu\n", string::npos);
  cout << "Using printf(\"%ld\"), you get:      ";
  printf("%ld\n", string::npos);
  return 0;
}
