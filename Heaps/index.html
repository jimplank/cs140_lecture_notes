<title>CS202 Lecture Notes</title>
<h2>CS202 Lecture Notes - Binary Heaps / Priority Queues</h2>
<UL>
<LI> James S. Plank
<LI> Directory: <b>/home/plank/cs202/Notes/Heaps</b>
<LI> Original notes for CS302: September 18, 2009
<LI> Modification to use interfaces in CS302: October 2, 2018.
<LI> Modification for CS202: November, 2021.
</UL>

The priority queue data structure maintains a collection of items with fast access to and removal
of the minimum.  A binary heap is a nice way to implement a priority queue.
In this lecture, we will do the following:

<UL>
<LI> Define the API, and talk about how to implement it with a multiset.
<LI> Describe a <i>binary heap</i>, how it implements a priority queue, and the
     implementation details that make it fast.
<LI> Show how you create a binary heap from a vector of elements in linear time.
<LI> Go over some implementation results and discuss some of the common pitfalls in
     computer science research.
</UL>

<hr>
<h3>API and Uses</h3>

Its API supports the following operations:
<p>
<UL>
<LI> A parameterless constructor creates an empty priority queue.
<LI> A constructor with a vector of values creates a priority queue containing those values.
<LI> <b>Push(v)</b> adds a value to the Priority Queue.
<LI> <b>Min()</b> returns the minimum value on the Priority Queue without changing the Priority Queue.
<LI> <b>Pop()</b> removes the minimum value from the Priority Queue and returns it to the caller.
<LI> <b>Size()</b> returns the number of elements in the priority queue.
<LI> <b>Empty()</b> returns whether the priority queue is empty.
<LI> <b>Clear()</b> removes all of the values from the priority queue.
</UL>
<P>

Priority queues are very natual data structures for <i>event-based simulation</i>.  In such
a simulation, you define events that occur at given times.  To run the simulation, you insert
each event into a priority queue (keyed on time).  Then you process the simulation by:

<UL>
<LI> Remove the event from the prioirity queue with the smallest time.
<LI> Process that event.  Often the event processing creates other events later in time.  Those
     events are added to the priority queue.
<LI> Repeat until the priority queue is empty, or until you declare that your simulation is over.
</UL>

As you can see, the two main operations are inserting (Push) onto a priority queue, and
returning the minimum (Pop).  In CS302, I used to give a lab that simulated cars and traffic
lights with an event-based simulation.  It was a fun lab, but it has given way to other labs....

<hr>
<h3>Implementation with a multiset</h3>

It should be clear to you that a C++ multiset can implement a Priority Queue very cleanly.
Suppose I have a <b>PQ</b> data structure that holds integers, and 
whose only member variable is a multiset of integers named <i>S</i>.
Then the <b>PQ</b> implementation is incredibly simple:

<p><center><table border=3 cellpadding=3><td><pre>
void   PQ::Push(int v) { S.insert(v); }
int    PQ::Pop(int v)  { int rv; rv = *S.begin(); S.erase(S.begin()); return rv; }
size_t PQ::Size()      { return S.size(); }
bool   PQ::Empty()     { return S.empty(); }
void   PQ::Clear()     { S.clear(); }

<font color=blue>// The default constructor can be used for the parameterless constructor.</font>
<font color=blue>// The default destructor works, too.</font>
<font color=blue>// You'll also note that the default copy constructor & assignment overload also work.</font>

PQ::PQ(const vector &lt;int&gt; &v) { size_t i; for (i = 0; i &lt; v.size(); i++) S.insert(v[i]); }
</pre></td></table></center><p>

Let's talk running times:

<UL>
<LI> <b>Push()</b> onto a priority queue with <i>n</i> elements: <i>O(log n)</i>.
<LI> <b>Pop()</b> on a priority queue with <i>n</i> elements: <i>O(log n)</i>.
<LI> <b>Size()</b>: <i>O(1)</i>.
<LI> <b>Empty()</b>: <i>O(1)</i>.
<LI> <b>Clear()</b>: <i>O(n)</i>.
<LI> Constructor without parameters: <i>O(1)</i>.
<LI> Constructor on a vector with <i>n</i> elements: <i>O(n log n)</i>.
<LI> Destructor on a priority queue with <i>n</i> elements: <i>O(n)</i>.
<LI> Copy constructor on a priority queue with <i>n</i> elements: <i>O(n)</i>.
<LI> Assignment overload on a priority queue with <i>n</i> elements: <i>O(n)</i>.
</UL>

That seems pretty efficient, doesn't it?

<hr>
<h3>Binary Heaps</h3>

Typically, one implements a priority queue with a data structure called a <i>binary heap</i>
(or simply <i>heap</i> for short).
A heap is a "complete binary tree" -- which means it is a tree where every level is full,
except for the last.  The last level fills up from the left to the right.
Thus, if we know how many elements are in a heap, we know exactly what it looks like.
For example, a heap with 10 elements will look as follows, regardless of the values of
the elements.

<p><center><table border=3 cellpadding=3><td valign=top><img src=img/Heap-10.png></td></table></center><p>

The other property of a heap is that the value of each node must be less than or equal to all of
the values in its subtrees.  That's it.  Below are two examples of heaps:

<p><center><table border=3 cellpadding=3>
<td valign=top><img src=img/Good-Heap-1.png></td>
<td valign=top><img src=img/Good-Heap-2.png></td>
</table></center><p>

That second one has duplicate values (two sixes), but that's ok.  It still fits the definition of a heap.
<p>
Below are three examples of non-heaps:
<p><center><table border=3 cellpadding=3>
<td valign=top align=center><img src=img/Bad-Heap-1.png><br>The 10 node is not smaller than the values in its subtrees.</td>
<td valign=top align=center><img src=img/Bad-Heap-2.png><br>Not a complete tree.</td>
<td valign=top align=center><img src=img/Bad-Heap-3.png><br>The last row is not filled from left to right.</td>
</table></center><p>

When we push a value onto a heap, we know where the new node is going to go.  So we insert the
value there.  Unfortunately, that may not result in a new heap, so we adjust the heap by "percolating up."
To percolate up, we look at the value's parent.  If the parent is smaller than the value, then we can
quit -- we have a valid heap.  Otherwise, we swap the value with its parent, and continue percolating up.
<p>
Below we give an example of inserting the value 2 into a heap:

<p><center><table border=3 cellpadding=3><tr>
<td valign=top align=center><u>Start</u><p><img src=img/Insert-Ex-1.png></td>
<td valign=top align=center><u>Put 2 into the new node and percolate up.</u><p><img src=img/Insert-Ex-2.png></td>
</tr><tr>
<td valign=top align=center><u>Continue percolating, since 2 is less than 3.</u><p><img src=img/Insert-Ex-3.png></td>
<td valign=top align=center><u>Now we stop, since 1 is less than 2.</u><p><img src=img/Insert-Ex-4.png></td></tr>
</table></center><p>

To pop a value off a heap, again, we know the shape of the tree when we're done -- we will lose the rightmost node
on the last level. So what we do is put the value in that node into the root (we will return the old value of the
root as the return value of the <b>Pop()</b> call).
Of course, we may not be left with a heap, so as in the previous example, we must modify the heap.  We do that
by "Percolating Down."  This is a little more complex than percolating up.  To percolate down, we check a value
against its children.  If it is the smallest of the three, then we're done.  Otherwise, we swap it with the child
that has the minimum value, and continue percolating down.  
<p>
As before, an example helps.  We will call <b>Pop()</b> on the last heap above.  This will return 1 to the user,
and will delete the node holding the 7, so we put the value 7 into the root and start percolating down:

<p><center><table border=3 cellpadding=3><tr>
<td valign=top align=center><u>Start</u><p><img src=img/Pop-Ex-1.png></td>
<td valign=top align=center><u>Swap 7 and 2</u><p><img src=img/Pop-Ex-2.png></td>
</tr><tr>
<td valign=top align=center><u>Swap 7 and 3.  We're Done.</u><p><img src=img/Pop-Ex-3.png></td>
<td valign=top align=center></td></tr>
</table></center><p>

If we think about running time complexity, obviously the depth of a heap with <i>n</i> elements is <i>log<sub>2</sub>(n)</i>.
Thus, <b>Push()</b> and <b>Pop()</b> both run with <i>O(log(n))</i> complexity.  This is the exact same as using 
a multiset above.  So why do we bother?
<p>
<hr>
<h3>Reason #1 Heaps are better than Multisets: Implementational Efficiency</h3>

The answer is that even though heaps and multisets have the same running time complexity, 
the heap implementation may be made  more
efficient than the multset.
Since we always add and subtract elements to and from the end of the heap, we may implement the heap
as a vector.  The root is at index 0, the nodes at the next level are at indices 1 and 2, etc.  For example,
we show how the final heap above maps to a vector:

<p><center><table border=3 cellpadding=3><tr>
<td valign=top align=center><img src=img/Linearize-1.png></td>
<td valign=center align=center><img src=img/Linearize-2.png></td>
</tr>
</table></center><p>

When we <b>Push()</b> an element onto the heap, we perform a <b>push_back()</b> on the vector, and then we 
percolate up.  When we <b>Pop()</b> an element off the heap, we store the root, replace it with the last
element, call <b>pop_back()</b> on the vector, and percolate down starting at the root.
A quick inspection of the indices shows that:
<p>
<UL>
<LI> We can find element <i>i</i>'s left child at index <i>i*2+1</i>.
<LI> We can find element <i>i</i>'s right child at index <i>i*2+2</i>.
<LI> We can find element <i>i</i>'s parent at index <i>(i-1)/2</i> - that uses integer division.
</UL>

Why is this more efficient than a multiset?  Think about memory efficiency.  The multiset is
implemented with a data structure like an AVL tree, where every node on the tree consumes 24 to
40 bytes of memory in addition to the value that is holding.  A vector only holds the value.
Moreover -- think about <b>insert()</b> on an AVL tree -- you'll need to do <i>(log n)</i>
comparisons and pointer assignments, then a <b>new</b>, a setting of four pointers, and then
potentially rebalancing.  With the heap, all you're doing is <b>push_back()</b> on the vector,
and then up to <i>(log n)</i> swaps.  The heap is definitely more efficient.

<hr>
<h3>Implementing Push / Percolate_Up</h3>

Let's go over the code to <b>push (and percolate_up)</b>.  To make it simple, I'm simply
writing a procedure <b>Push()</b> that takes a heap in the form of a vector (reference
parameter, so that I change it), and a value to insert onto the heap:

<b><a href=src/push.cpp>src/push.cpp</a></b>

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* This program implements Push() on a heap.
   Its main reads a vector of integers on standard input, and then pushes them all onto
   a heap, printing the heap each time. */</font>

#include &lt;vector&gt;
#include &lt;cstdio&gt;
#include &lt;cstdlib&gt;
#include &lt;iostream&gt;
using namespace std;

<font color=blue>/* Push the value val onto the heap. */</font>

void Push(vector &lt;int&gt; &heap, int val)
{
  int i, p;

  i = heap.size();
  heap.push_back(val);     <font color=blue>// Put the value onto the end of the heap.</font>

  while (i &gt; 0) {          <font color=blue>// Percolate up, starting at i.</font>
    p = (i-1) / 2;         <font color=blue>// You stop when you reach the root, or when the parent's value</font>
    if (heap[p] &gt; val) {   <font color=blue>// is less than or equal to val.</font>
      heap[i] = heap[p];
      heap[p] = val;
      i = p;
    } else {
      return;
    }
  }
}

int main()
{
  vector &lt;int&gt; v;
  vector &lt;int&gt; heap;
  int val;
  size_t i, j;

  <font color=blue>/* Read values into a vector */</font>

  while (cin &gt;&gt; val) v.push_back(val);

  <font color=blue>/* Repeatedly call Push and print the heap. */</font>

  for (i = 0; i &lt; v.size(); i++) {
    Push(heap, v[i]);
    printf("Push %2d : ", v[i]);
    for (j = 0; j &lt; heap.size(); j++) printf(" %2d", heap[j]);
    printf("\n");
  }
  return 0;
}
</pre></td></table></center><p>

To illustrate the heap working, we'll insert numbers the numbers
7, 4, 22, 1, 7, 8, 9, 6, 3, 10 and 2:

<pre>
UNIX> <font color=darkred><b>echo 7 4 22 1 7 8 9 6 3 10 2 | bin/push</b></font>
Push  7 :   7
Push  4 :   4  7
Push 22 :   4  7 22
Push  1 :   1  4 22  7
Push  7 :   1  4 22  7  7
Push  8 :   1  4  8  7  7 22
Push  9 :   1  4  8  7  7 22  9
Push  6 :   1  4  8  6  7 22  9  7
Push  3 :   1  3  8  4  7 22  9  7  6
Push 10 :   1  3  8  4  7 22  9  7  6 10
Push  2 :   1  2  8  4  3 22  9  7  6 10  7
UNIX> <font color=darkred><b></b></font>
</pre>

You should be able to see that the last two lines are the two heaps from our insertion
example above:

<p><center><table border=3><td><img src=img/Insert-Ex-1.png></td><td><img src=img/Insert-Ex-4.png></td></table></center><p>

<hr>
<h3>Pop()</h3>

I'm not going to implement <b>Pop()</b> here due to time.  It's more complex than <b>Push()</b>,
because:
<UL>
<LI> <b>Push()</b> needs to compare a node with its parent.
<LI> <b>Pop()</b> needs to compare a node with both of its children (if it has children).
</UL>

It still has the same running time complexity.

<hr>
<h3>Constructing a heap from a vector</h3>

The final piece of the puzzle is how you construct the heap
from a vector of values.  Obviously, you could simply <b>Push()</b> each element 
of the vector, which would make the constructor <i>O(n(log(n)))</i>.
However, you can do better.  Let us do the following instead:
<p>
<UL> 
<LI> Create the binary tree directly from the vector.
<LI> Consider all the nodes at the bottom level of the tree.  Each of these nodes is a valid heap.
<LI> Now consider all the nodes at the next-to-bottom level of the tree.  If we call 
<b>Percolate_Down()</b>
on each of these nodes, then each of them will be a heap.
<LI> We keep doing this to each successively higher level of the tree until we call 
<b>Percolate_Down()</b>
on the root, and we are left with a valid heap.
</UL>
<p>

Below is an example.  Here's the vector that we want to turn into a heap:

<p><center><table border=3 cellpadding=3>
<td align=center width=20>77</td>
<td align=center width=20>21</td>
<td align=center width=20>55</td>
<td align=center width=20>94</td>
<td align=center width=20>1</td>
<td align=center width=20>27</td>
<td align=center width=20>7</td>
<td align=center width=20>67</td>
<td align=center width=20>61</td>
<td align=center width=20>78</td>
</table></center><p>

We convert the vector into a tree, as pictured below, and then we call <b>Percolate_Down()</b> 
on nodes
of each successively higher level.  These are the nodes pictured in yellow.  By the time we
get to the root, we have created a valid heap:


<p><center><table border=3 cellpadding=3>
<tr>
<td valign=top align=center><img src=img/Linear-1.png></td>
<td valign=center align=center><img src=img/Linear-2.png></td>
</tr>
<tr>
<td valign=top align=center><img src=img/Linear-3.png></td>
<td valign=center align=center><img src=img/Linear-4.png></td>
</tr>
</table></center><p>

To analyze the running time complexity, we need a little math.  Suppose our tree is complete at each
level.  The tree has <i>n</i> nodes, and thus its depth is <i>log(n)</i>.  The worst case performance
of <b>Percolate_Down()</b> for a node at level <i>i</i> is <i>log(n)-i</i>.  Thus, the performance of 
<b>Percolate_Down()</b>  for a node at the bottom level is 1;  for a node at the penultimate level, it is 2,
etc.  
<p>
The bottom level contains <i>(n+1)/2</i>  nodes, which is roughly <i>n/2</i>.  
Thus, all of the <b>Percolate_Down()</b>'s take <i>(1)(n/2)</i> = <i>n/2</i> operations.
At the next level, there are <i>n/4</i> nodes, whose <b>Percolate_Down()</b>'s take 2 operations each.
Thus, all of them take <i>2n/4</i>.  At the next level, there are <i>n/8</i> nodes whose
<b>Percolate_Down()</b>'s take 3 operations each.  Thus, all of them take <i>3n/8</i>.  Do you see the
pattern?   
<p>
The creation of the heap can be converted into a summation:
<p><center><table border=0 cellpadding=3><td valign=top><img src=img/Eq1.png></td></table></center><p>
To figure out what that second summation is, let's first write a quick C++ program to calculate its
value for the first 15 values of <i>n</i>.  The code is below 
(<b><a href=src/formula.cpp>src/formula.cpp</a></b>), and to the right, we show its output:

<p><center><table border=3 cellpadding=3><td valign=top><pre>
#include &lt;iostream&gt;
using namespace std;

int main()
{
  double num, den, total, n;

  num = 1;
  den = 2;
  total = 0;
  
  for (n = 0; n &lt; 15; n++) {
    total += (num/den);
    num++;
    den *= 2;
    cout &lt;&lt; total &lt;&lt; endl;
  }
}
</pre></td><td valign=top width=200><pre>
UNIX> <font color=darkred><b>bin/formula</b></font>
0.5
1
1.375
1.625
1.78125
1.875
1.92969
1.96094
1.97852
1.98828
1.99365
1.99658
1.99817
1.99902
1.99948
UNIX> <font color=darkred><b></b></font>
</pre></td></table></center><p>

Looks like it converges to 2.  Let's analyze it mathematically.  Consider the following sum:

<p><center><table border=0 cellpadding=3><td valign=top><img src=img/Eq2.png></td></table></center><p>

Now, let's consider the equation <i>G - G/2</i>:
<p><center><table border=0 cellpadding=3><td valign=top><img src=img/Eq3.png></td></table></center><p>

From high school math, we know that this last summation equals one, so <i>G - G/2 = 1</i>, which 
means that <i>G = 2</i>.
<hr>
<h3>What is the upshot of all of this??</h3>

The upshot is that creating a heap from a vector is less than or equal to <i>Gn</i>, which means that
it is <i>O(n)</i> instead of <i>O(n(log(n)))</i>.  Is that significant?  
Well...
<hr> 
<h3> Performance Testing</h3>

This will be a brief section.  I will not test you on this, but keep it in the back of 
your mind.  When I went to performance test this for CS302, I wrote a program that read
numbers from standard input into a vector, and then called the priority queue constructor on the vector.
It then called <b>Pop()</b> until the priority queue was empty, to print the numbers in sorted order.
I had three variants of this program, that differed in their priority queue implementations:

<UL>
<LI> Multiset: <I>O(n log n)</i>
<LI> Constructor that simply inserts everything: <I>O(n log n)</i>
<LI> The <i>O(n)</i> constructor.
</UL>

Here's a graph -- I wrote a program <b>genrandom</b> which generated random values and printed
them on standard output.

<p><center><table border=3 cellpadding=3><td valign=top><img src=img/sort_test1.jpg width=500></td></table></center><p>

That graph sucks.  I include it because I see students make graphs like these, and they need
to have a more critical eye.  
Problem #1 is the squiggly lines -- what they indicate is that 
the individual data points have some variance, and that we should run more than one run
per test, averaging the results.  Still, if we ignore the squiggly lines, we draw two conclusions:

<OL>
<LI> The heap implementations are faster than the set implementation.
<LI> There is no appreciable difference between the two heap implementations.
</OL>

Is the second conclusion surprising?  Let's think about it some more.  What's going on 
when we are testing 100,000 numbers?

<UL>
<LI> We are generating and printing 100,000 numbers in <b>bin/genrandom</b>.
<LI> These characters are sent via the operating system as standard input to the heap program.
<LI> The heap program sorts them and prints them.
<LI> The operating system eats the output.
</UL>

We are timing a lot of things here, in addition to what we want to be timing. 
In particular, we are timing a lot of I/O to and from the testing program.
Let's instead try timing just what we want to time -- the time to create the heap.

<p>
Instead, I wrote a second program that creates the random vector, calls the constructor
and exits.  I made it so that it could run for multiple iterations, so that if it was
too fast, the multiple iterations allowed me to time it effectively.
<p>

<p><center><table border=3 cellpadding=3><td valign=top><img src=img/sort_test2.jpg width=500></td></table></center><p>

There -- that shows what we expect to see.  It does make a difference!
<p>
I show this test to let you know that
experimental computer science research
can be very tricky.  We had a simple goal: to demonstrate that a <i>O(n)</i> algorithm
runs faster than a <i>O(n(log(n)))</i> one.  We tried an obvious solution, and we couldn't
use it because we weren't really testing what we wanted to test.  When we rewrote our
testing programs to do a better job of removing the noise from the experiment, we were
able to demonstrate improved performance.  
